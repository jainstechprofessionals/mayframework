
public class UnaryOperators {

	public static void main(String[] args) {

		int a = 10;
		int temp;
		// Unary
//		1. Postfix  : exp++,exp--
//		2. Prefix : ++exp,--exp		

//		temp =a++;  // temp = 10;
//		temp =++a; // temp = 11;
//		temp = a++ + ++a - a-- - a++; // temp = 10 + 12-12 - 11;
//		temp = ++a + ++a + a++;  // temp = 11 + 12 + 12;
//		temp = --a - ++a + --a; // temp=10 - 11 + 10;
//		temp = a++ + ++a - --a + a++; // temp =10 + 12 - 11 + 11 
		temp = ++a - --a - --a - a; // temp = 11 - 10 - 9 - 9;
		
		System.out.println(a);
		System.out.println(temp);

	}

}
