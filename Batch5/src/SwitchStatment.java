
public class SwitchStatment {

	public static void main(String[] args) {
		int a = 109;
        int b=3;
        int temp=0;
		int n=1;
		
		switch (n) {

		case 1:
			temp=a+b;
			System.out.println(temp);
			System.out.println("a");
			System.out.println("in jan");
			break;
		case 2:
			temp=a-b;
			System.out.println("in feb");
			break;
		case 3:
			System.out.println("in mar");
			break;

		default:
			System.out.println("please enter a valid month");

		}

	}

}
