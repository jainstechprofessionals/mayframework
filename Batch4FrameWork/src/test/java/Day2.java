import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebElement;

public class Day2 {
	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "\\src\\test\\resources\\chromedriver.exe");
		WebDriver driver;
		driver = new ChromeDriver();		 
		driver.get("https://www.seleniumeasy.com/test/basic-first-form-demo.html");
		driver.findElement(By.id("user-message")).sendKeys("rounak");
		
		By showMessage = By.xpath("//button[text()='Show Message']");	
		WebElement we = driver.findElement(showMessage);
		we.click();		
		String str =driver.findElement(By.id("display")).getText();		
		System.out.println(str);
		
		
	
	}
}


