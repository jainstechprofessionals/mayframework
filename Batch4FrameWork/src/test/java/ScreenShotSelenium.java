import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ScreenShotSelenium {

	public static void main(String[] args) throws IOException {

		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "\\src\\test\\resources\\chromedriver.exe");
		WebDriver driver;
		driver = new ChromeDriver();
		driver.get("https://demo.testfire.net/login.jsp");

		TakesScreenshot scrSht = (TakesScreenshot) driver;

		File srcFile = scrSht.getScreenshotAs(OutputType.FILE);

		File des = new File("C:\\Users\\dell\\eclipse-workspace1\\Batch4FrameWork\\rounak.jpeg");

		FileUtils.copyFile(srcFile, des);
	}

}
