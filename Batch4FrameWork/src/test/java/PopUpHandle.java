import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class PopUpHandle {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "\\src\\test\\resources\\chromedriver.exe");
		WebDriver driver;
		driver = new ChromeDriver();

		driver.get("https://www.seleniumeasy.com/test/javascript-alert-box-demo.html");
		driver.manage().window().maximize();
//		driver.findElement(By.xpath("(//button[text()='Click me!'])[1]")).click();
//		Alert alt= driver.switchTo().alert();
//		System.out.println(alt.getText());
//		Thread.sleep(4000);
//		alt.accept();

//		driver.findElement(By.xpath("(//button[text()='Click me!'])[2]")).click();
//		Alert alt = driver.switchTo().alert();
//		System.out.println(alt.getText());
//		Thread.sleep(4000);
//		alt.accept();
//		System.out.println(driver.findElement(By.id("confirm-demo")).getText());
//
//		driver.findElement(By.xpath("(//button[text()='Click me!'])[2]")).click();
//		alt = driver.switchTo().alert();
//		System.out.println(alt.getText());
//		Thread.sleep(4000);
//		alt.dismiss();
//		System.out.println(driver.findElement(By.id("confirm-demo")).getText());

		driver.findElement(By.xpath("//button[text()='Click for Prompt Box']")).click();
		Alert alt = driver.switchTo().alert();
		System.out.println(alt.getText());
		Thread.sleep(4000);
		alt.sendKeys("rounak");
		alt.accept();
		System.out.println(driver.findElement(By.id("confirm-demo")).getText());
		
		
		driver.findElement(By.xpath("//button[text()='Click for Prompt Box']")).click();
		 alt = driver.switchTo().alert();
		System.out.println(alt.getText());
		Thread.sleep(4000);
		alt.sendKeys("rounak");
		alt.dismiss();
		System.out.println(driver.findElement(By.id("confirm-demo")).getText());
		
		
		
	}

}
