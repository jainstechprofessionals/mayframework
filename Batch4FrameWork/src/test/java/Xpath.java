
public class Xpath {

	// Tag : <input>
	// Tag with Attribute : <input type = "rounak" >
	
//	************ Xpath ******************
	
	// Absolute (Static) (/)
	// Relative (Dynamic ) (//)
	
//	1. Basic Xpath  : //input[@id='uid']
//  2. Text() : //h1[text()='Online Banking Login']
//	3. Starts-with : //h1[starts-with(text(),'Online Banking')]
//	4. contains : //span[contains(text(),'username or password')] 	
		//span[contains(@id,'Content_Main_message')]
// 5. To go on parent or one node above : //span[contains(@id,'Content_Main_message')]/..
	
//	6. following-sibling : //td[contains(text(),'Username')]/following-sibling::td/input[@id='uid']
// 7. preceding-sibling : //input[@id='uid']/../preceding-sibling::td
//	8. Child : //form[@action='doLogin']/table/tbody/tr/child::td
//	9. Tag index : //form[@action='doLogin']/table/tbody/tr[1]/td
// 10. PArent : //input[@id='uid']/parent::td
	// 11. ancestor : //input[@id='uid']/ancestor::div
	// 12. Xpath index : (//a[contains(text(),'Deposit')])[2]
	
	
	
}
