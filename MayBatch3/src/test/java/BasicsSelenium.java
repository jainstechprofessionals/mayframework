import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BasicsSelenium {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "\\src\\test\\resources\\chromedriver.exe");

		WebDriver driver;
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.seleniumeasy.com/test/basic-first-form-demo.html");

//		driver.findElement(By.xpath("//input[@id='user-message']")).sendKeys("rounak jain");
//
//		driver.findElement(By.xpath("//button[text()='Show Message']")).click();
//		String str = driver.findElement(By.id("display")).getText();
//		System.out.println(str);
		
		
		driver.findElement(By.id("sum1")).sendKeys("20");
		driver.findElement(By.id("sum2")).sendKeys("10");
		driver.findElement(By.xpath("//button[text()='Get Total']")).click();
		String str1 = driver.findElement(By.id("displayvalue")).getText();
		System.out.println(str1);
		
		driver.quit();
	}

}
