import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class HandleSelectDD {

	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "\\src\\test\\resources\\chromedriver.exe");

		WebDriver driver;
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.seleniumeasy.com/test/basic-select-dropdown-demo.html");

//		WebElement we = driver.findElement(By.id("select-demo"));
//		
//		Select sel = new Select(we);
////		sel.selectByIndex(1);
////		Thread.sleep(3000);
////		sel.selectByValue("Wednesday");
////		Thread.sleep(3000);
////		sel.selectByVisibleText("Friday");
//		
//		List<WebElement> lst = sel.getOptions();
//		
//		for(int i=0;i<lst.size();i++) {
//			String str1 = lst.get(i).getText();
//			System.out.println(str1);
//		}

		WebElement multiList = driver.findElement(By.id("multi-select"));
		Select sel = new Select(multiList);

		sel.selectByIndex(1);
		sel.selectByIndex(2);
		sel.selectByIndex(3);
		Thread.sleep(4000);
		sel.deselectByIndex(2);

	}

}
