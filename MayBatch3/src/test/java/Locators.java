
public class Locators {

	// DOM : Documents object model
	// Id
	// Name
	// link text
	// partial link text
	// class

//	**************
	// xpath
	// Absolute (Static) : /
	// Relative (Dynamic) : //tagname[@attribute='value']
//	1. Basic xpath : //input[@id='uid']
//	2. text() : //a[text()='Login'], //a[text()='Checking']
	// 3. go to parent : //a[text()='Checking']/..  , //a[text()='Checking']/../../..
//	4. through : //ul[@class='sidebar']/li
	//5. child :  a) //ul[@class='sidebar']/child::li/a[@id='MenuHyperLink1']
	
//	b) //ul[@class='sidebar']/child::li
//	c) //ul[@class='sidebar']/child::li[1]
	
	// 5. contains :  //h1[contains(text(),'Online Banking')]
	// 6. starts-with : //h1[starts-with(text(),'Online')]
 	// 7 . following-sibling ://td[contains(text(),'Username')]/following-sibling::td/input
	
	// 8. preceding-sibling : //input[@id='uid']/../preceding-sibling::td
	// 9. parent : //input[@id='uid']/parent::td
	//10. ancestor : //input[@id='uid']/ancestor::tr
	// 11. index : (//*[contains(text(),'Deposit Product')])[2]
	
	

}
