import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class PopUpHandle {

	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "\\src\\test\\resources\\chromedriver.exe");

		WebDriver driver;
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.seleniumeasy.com/test/javascript-alert-box-demo.html");

		
//		******************** Normal Alert***************
		
		
//		driver.findElement(By.xpath("(//button[text()='Click me!'])[1]")).click();
		
//		Alert alt = driver.switchTo().alert();
//		String str =alt.getText();
//		System.out.println(str);
//		Thread.sleep(3000);
//		alt.accept();
		
		
//		******************** Confirmation Alert***************
		
//		driver.findElement(By.xpath("(//button[text()='Click me!'])[2]")).click();	
//		Alert alt = driver.switchTo().alert();
//		String str =alt.getText();
//		System.out.println(str);
//		Thread.sleep(3000);
//		alt.accept();
//		String str1 = driver.findElement(By.id("confirm-demo")).getText();
//		System.out.println(str1);
//		Thread.sleep(3000);
//		
//		driver.findElement(By.xpath("(//button[text()='Click me!'])[2]")).click();	
//		 alt = driver.switchTo().alert();
//		 str =alt.getText();
//		System.out.println(str);
//		Thread.sleep(3000);
//		alt.dismiss();
//		 str1 = driver.findElement(By.id("confirm-demo")).getText();
//		System.out.println(str1);
		
//		******************** prompt Alert***************
		
		driver.findElement(By.xpath("(//button[text()='Click for Prompt Box'])")).click();	
		Alert alt = driver.switchTo().alert();
		String str =alt.getText();
		System.out.println(str);
		Thread.sleep(3000);
		alt.sendKeys("rounak");
		alt.accept();
		String str1 = driver.findElement(By.id("prompt-demo")).getText();
		System.out.println(str1);
		Thread.sleep(3000);
		
		driver.findElement(By.xpath("(//button[text()='Click for Prompt Box'])")).click();	
		 alt = driver.switchTo().alert();
		 str =alt.getText();
		System.out.println(str);
		Thread.sleep(3000);
		alt.dismiss();
		 str1 = driver.findElement(By.id("prompt-demo")).getText();
		System.out.println(str1);
		
		
		
		
		
		
	}

	
}
